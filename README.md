# NFT MARKETPLACE UPGRABLE [ Whitelisting and BlackListing of Address and Tokens ]

## Pre - Requistes
- Truffle
- Ganache

## To do Things Before Deploy
- Open Ganache and copy 1st Three address of ganache and paste that address in `test/NFTMARKETPLACEUPGRABLE.test.js file .`
- Delete .openzepplein folder and build folder.. [If it is found in your VSCODE ]

## Run Commands 
- For Compile ` truffle compile `.
- For Run    `truffle migrate --network development`
- To Run Test `truffle test`.

## Screenshots
![Screesnshots](Screenshots/SS.png)