// SPDX-License-Identifier: MIT

pragma solidity ^0.8.0;

import "@openzeppelin/contracts-upgradeable/utils/ContextUpgradeable.sol";
import "@openzeppelin/contracts-upgradeable/proxy/utils/Initializable.sol";

/**
 * @dev Contract module which allows children to implement an emergency stop
 * mechanism that can be triggered by an authorized account.
 *
 * This module is used through inheritance. It Creates a security for address and Token Transfer, 
 * When the are addresses and Tokens are restriced to transfer by ADMIN's OR CONTRACT Owners.
 */
abstract contract WhitelistandBlacklist is Initializable, ContextUpgradeable {
    
    /**
     * @dev Emitted when the Address is added to White List is triggered by `account`.
     */
    
     event AddressWhiteListed(address account);

    /**
     * @dev Emitted when the Token is added to White List is triggered by `account`.
     */

     event TokenWhiteListed(uint tokenid);
    
    /**
     * @dev Emitted when the Address is added to Black List is triggered by `account`.
     */
    event AddressBlackListed(address account);

    /**
     * @dev Emitted when the Token is added to White List is triggered by `account`.
     */
    event TokenBlackListed(uint tokenid);


     /**
     * @dev Mapping Varibales That are used add Addresses and tokens to WhiteList and BlackList.
     */

      /**
     * @dev By default every mapping address and token will be false state..
     */

    mapping (address => bool) public isAddressWhiteListed;
    mapping (address => bool) public isAddressBlackListed;

    mapping (uint => bool) public isTokensWhiteListed;
    mapping (uint => bool) public isTokensBlackListed;

    mapping (uint => bool) public isTransferable;

    /**
     * @dev Initializes the WhitelistandBlacklist contract  .
     */
    function __WhitelistandBlacklist_init() internal onlyInitializing {
        __WhitelistandBlacklist_init_unchained();
    }

    function __WhitelistandBlacklist_init_unchained() internal onlyInitializing {
       
    }



     /**
     * @dev Modifier to make a function callable only when adding address to Whitelist.
     *
     * Requirements:
     *
     * - The Address must not be in Blacklist.
     */
    modifier CheckAddressIsBlackListed(address _address) {
        require(!isAddressBlackListed[_address], "Failed: Your Address is BlackListed");
        _;
    }

     /**
     * @dev Modifier to make a function callable only when adding Token to Whitelist.
     *
     * Requirements:
     *
     * - The Token must not be in Blacklist.
     */
     modifier CheckTokenIsBlackListed(uint _tokenid) {
        require(!isTokensBlackListed[_tokenid], "Your Token is Black Listed");
        _;
    }

     /**
     * @dev Modifier to make a function callable only when Removing address  from Blacklist.
     *
     * Requirements:
     *
     * - The Address already must be in Blacklisted in order to remove Adrress from blacklist.
     */
     modifier AlreadyTokenIsBlackListed(uint _tokenid) {
        require(isTokensBlackListed[_tokenid], "Your Token is not BlackListed");
        _;
    }

      /**
     * @dev Modifier to make a function callable only when Removing Token from Blacklist.
     *
     * Requirements:
     *
     * - The Token already must be in Blacklisted in order to remove Token from blacklist.
     */
    modifier AlreadyAddressIsBlackListed(address _address) {
        require(isAddressBlackListed[_address], "Failed: Your Address is not in BlackList");
        _;
    }
    

    /**
     * @dev Sets true if the Addresses are added to WhiteList Mapping.
     * It also Checks for Address is already blacklisted.
     * If it is blacklisted it will not add Address to whitelist.
     */
    function AddwhiteListAddresses(address _address) public CheckAddressIsBlackListed(_address){
        isAddressWhiteListed[_address]=true;
        emit AddressWhiteListed(_msgSender());
    }

    /**
     * @dev Sets true if the Addresses are added to BlackList Mapping.
     */
    function AddblackListAddresses(address _address) public {
        isAddressBlackListed[_address]=true;
        emit AddressBlackListed(_msgSender());
    }


    /**
     * @dev Sets true if the Tokens are added to WhiteList Mapping.
     * It also Checks for Token is already blacklisted.
     * If it is blacklisted it will not add token to whitelist.
     */
    function AddwhiteListTokens(uint _tokenid)public CheckTokenIsBlackListed(_tokenid) {
        isTokensWhiteListed[_tokenid]=true;
        emit TokenWhiteListed(_tokenid);
    }

    /**
     * @dev Sets true if the Tokens are added to BlackList Mapping.
     */
    function AddblackListTokens(uint _tokenid)public{
        isTokensBlackListed[_tokenid]=true;
        emit TokenBlackListed(_tokenid);
    }

    /**
     * @dev Sets true and the Tokens are Eligible to Transfer.
     */
    function AddisTransferableToken(uint _tokenid)public{
        isTransferable[_tokenid]=true;   
    }




     /**
     * @dev Sets false if the Addresses are added to WhiteList Mapping.
     * and asssumed as address is removed from WhiteList.
     */
    function RemovewhiteListAddresses(address _address) public{
        isAddressWhiteListed[_address]=false;
    }

     /**
     * @dev Sets false if the Addresses are added to BlackList Mapping.
      * It also Checks for Token is already blacklisted.
     * If it is blacklisted it will be removed from blacklist.
     * and asssumed as address is removed from BlackList.
     */
    function RemoveblackListAddresses(address _address) public AlreadyAddressIsBlackListed(_address){
        isAddressBlackListed[_address]=false;
    }

    /**
     * @dev Sets false if the Tokens are added to WhiteList Mapping.
     * and asssumed as Token is removed from WhiteList.
     */
    function RemovewhiteListTokens(uint _tokenid)public{
        isTokensWhiteListed[_tokenid]=false;
    }

     /**
     * @dev Sets false if the Tokens are added to BlackList Mapping.
     * It also Checks for Token is already blacklisted.
     * If it is blacklisted it will be removed from blacklist.
     * and asssumed as Token is removed from BlackList.
     */
    function RemoveblackListTokens(uint _tokenid)public AlreadyTokenIsBlackListed(_tokenid){
        isTokensBlackListed[_tokenid]=false;
    }

    /**
     * @dev Sets false and the Tokens are not Eligible to Transfer.
     */
    function RemoveisTransferableToken(uint _tokenid)public{
        isTransferable[_tokenid]=false;
    }

    /**
     * @dev This empty reserved space is put in place to allow future versions to add new
     * variables without shifting down storage in the inheritance chain.
     * See https://docs.openzeppelin.com/contracts/4.x/upgradeable#storage_gaps
     */
    uint256[49] private __gap;
}